function showData(){
				var totalValue= 0;
				$.get(
						$('[data-getDatafield]').prop('value'),
						function(data){
						
								var product = [];
				                $.each(data, function (index, value) {
				                    product.push('<tr>');
				                    product.push('<td>'+value.name+'</td>')
				                    product.push('<td>'+value.quantity+'</td>')
				                    product.push('<td>'+value.price+'</td>')
				                    product.push('<td>'+value.date+'</td>')
				                    product.push('<td>'+value.quantity * value.price+'</td>')
				                    product.push('</tr>');

				                    totalValue = totalValue + (value.quantity * value.price);
				                });

				                	product.push('<tr>');
				                    product.push('<td></td>')
				                    product.push('<td></td>')
				                    product.push('<td></td>')
				                    product.push('<td></td>')
				                    product.push('<td>'+totalValue+'</td>')
				                    product.push('</tr>');
				                $('[data-showAllData]').html(product.join(''));
						}
					)
			}

			showData();


			$('[data-addProduct="true"]').on( 'submit', function() {	
	        $.post(
	            $(this).prop('action'),
	            {
	                "_token": $(this).find( 'input[name=_token]' ).val(),
	                "quantity":  $(this).find( 'input[name=quantity]' ).val(),
	                "name":  $(this).find( 'input[name=name]' ).val(),
	                "price":  $(this).find( 'input[name=price]' ).val(),
	            },
	            function( data ) {
	            	if(data.status =="success"){

	            		alert(data.message);
	            		showData();
	            	}else{
	            		alert('Some Problem Occur Please try Again');
	            	}
	            },
	            'json'
	        );
	        return false;
	    });