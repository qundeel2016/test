<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'getProduct', 'uses'=>'ProductController@index']);

Route::post('/', ['as'=>'postProduct', 'uses'=>'ProductController@create']);

Route::get('/product',['as'=>'getProductData', 'uses'=>'ProductController@showData']);
