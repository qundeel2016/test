<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
	/*
	|get function to show product page
	*/
	public function index(){
    	return view('index');
	}

	public function showData(){
		$data = rtrim(\Storage::get('product.json'), ',');
    	$product = '['. $data. ']';

    	return response()->json(json_decode($product));
	}
	/*
	|post function get data from form add append to json file
	|After data add display successfull messsage to users
	*/
    public function create(Request $request){
    	$data = json_encode([
    		"name" => $request->get('name'),
    		"price" =>$request->get('price'),
    		"quantity" =>$request->input('quantity'),
    		"date" => date('Y-m-d h:i:s A'),
    	]);
    	\Storage::append('product.json', $data . ',');

    	return response()->json([
    		'status'=>'success',
    		'message'=>'Successfully Add Data',
    	]);	
    }
}
