<!DOCTYPE html>
<html lang="en">
    <head> 
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		<!-- Website CSS style -->
		<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">

		<!-- Website Font style -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
		
		<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

		<title>Product Add</title>
	</head>
	<body>
		<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Product Add</h1>
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
					<form class="form-horizontal" method="post" action="{{route('postProduct')}}" data-addProduct="true">
						
						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Product Name</label>
							<div class="cols-sm-10">
								
									<input type="text" class="form-control" name="name" id="name"  placeholder="Enter your Name"/ required >
							
							</div>
						</div>
						{{csrf_field()}}

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Quantity in stock</label>
							<div class="cols-sm-10">
							
									<input type="number" class="form-control" name="quantity" id="quantity"  placeholder="Quantity"/  min="1" required >
							
							</div>
						</div>

						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Price per item</label>
							<div class="cols-sm-10">
								
									<input type="number" class="form-control" name="price" id="price"  placeholder="Price per item"/ min="1" required >
								
							</div>
						</div>

						<div class="form-group ">
							<button type="submit" class="btn btn-primary btn-lg btn-block login-button">Add Product</button>
					
					</form>
				</div>
			</div>
			<br>
			<table class="table" style="background-color: #fff;">
				  <thead>
				    <tr>
				      
				      <th>Product Name</th>
				      <th>Quantity in stock</th>
				      <th>Price pet item</th>
				      <th>DateTime</th>
				      <th>Total Value number</th>
				    </tr>
				  </thead>
				  <tbody data-showAllData>
				  
				  </tbody>

				</table>

				<input type="hidden" value="{{route('getProductData')}}" data-getDatafield />

		</div>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="{{asset('js/ajax.js')}}"></script>

	</body>
</html>